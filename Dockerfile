FROM debian:11.3
COPY  . /srv
WORKDIR /srv
RUN apt update
RUN apt-get upgrade -y
RUN apt-get install htop -y
RUN apt-get install python3 -y
RUN apt-get install python3-pip -y
RUN pip3 install -r requirements.txt
CMD [ "gunicorn" , "--bind=0.0.0.0:9090", "hello:app" ]
